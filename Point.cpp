#include "Point.h"
#include <cmath>

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point Point::operator+(const Point& other) const
{
	Point myPoint(this->_x + other._x, this->_y + other._y);
	return myPoint;
}

Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(this->_x - other._x, 2) + pow(this->_y - other._y, 2));
}