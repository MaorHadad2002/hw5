#include "Triangle.h"

void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<(unsigned char)>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name)
{
	this->_p1 = a;
	this->_p2 = b;
	this->_p3 = c;
}

double Triangle::getPerimeter() const
{
	int d1 = sqrt((pow(this->_p2.getX() - this->_p1.getX(), 2) + pow(this->_p2.getY() - this->_p1.getY(), 2)));
	int d2 = sqrt((pow(this->_p3.getX() - this->_p2.getX(), 2) + pow(this->_p3.getY() - this->_p2.getY(), 2)));
	int d3 = sqrt((pow(this->_p1.getX() - this->_p3.getX(), 2) + pow(this->_p1.getY() - this->_p3.getY(), 2)));

	return d1 + d2 + d3;
}

double Triangle::getArea() const
{
	double height = this->_p3.getY() - this->_p1.getY();
	double length = abs(this->_p1.getX() - this->_p2.getX());

	return height * length / 2;
}