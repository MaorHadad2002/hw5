#include "Arrow.h"
#include <cmath>

void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}

Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name)
{
	this->_p1 = a;
	this->_p2 = b;
}

double Arrow::getPerimeter() const
{
	return sqrt(pow(this->_p1.getX() - this->_p2.getX, 2) + pow(this->_p1.getY() - this->_p2.getY(), 2));
}