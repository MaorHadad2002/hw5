#include "Menu.h"

int main()
{
	int choice = 0;
	std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << std::endl;
	std::cin >> choice;

	switch (choice)
	{
		case 0:
			//addShape()
			break;
		case 1:
			//modifyShape()
			break;
		case 2:
			//deleteShapes()
			break;
		case 3:
			//Exit
			break;
		default:
			break;
	}
	getchar();
	return 0;
}

void addShape()
{
	int choice = 0;
	std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << std::endl;
	switch (choice)
	{
		case 0:
			//addCircle()
			break;
		case 1:
			//addArrow()
			break;
		case 2:
			//addTriangle()
			break;
		case 3:
			//addRectangle()
			break;
		default:
			break;
	}
}