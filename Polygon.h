#pragma once

#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
#include "Cimg.h"

using namespace std;

class Polygon : public Shape
{
public:
	Polygon();
	Polygon(const string& type, const string& name);
	virtual ~Polygon();

	virtual double getPerimeter() const = 0;
	virtual double getArea() const = 0;

protected:
	vector<Point> _points;
};