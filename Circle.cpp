#include "Circle.h"


void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
}

Circle::Circle(const Point& center, double radius, const string& type, const string& name)
{
	this->center = center;
	this->radius = radius;
}

const Point& Circle::getCenter() const
{
	return this->center;
}

double Circle::getRadius() const
{
	return this->radius;
}

double Circle::getPerimeter() const
{
	return 2 * PI * this->radius;
}

double Circle::getArea() const
{
	return pow(this->radius, 2) * PI;
}